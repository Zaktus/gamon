/* globals __DEV__ */
import Phaser from 'phaser'
import Wheel from '../sprites/Wheel'






export default class extends Phaser.State {
  init () {}
  preload () {
  }

  create () {

      // giving some color to background
      this.game.stage.backgroundColor = "#880044";
      this.wheel = this.add.existing(new Wheel([1,1,1,1,1,1,1,1],this.game));

      this.wheel.x = this.game.width / 2;
      this.wheel.y = this.game.height / 2;

      this.input.onDown.add(this.spin, this);




  }


    spin(){
      this.wheel.spin();
    }
    // function to assign the prize



  render () {
    if (__DEV__) {
      //this.game.debug.spriteInfo(this.mushroom, 32, 32)
    }
  }
}
