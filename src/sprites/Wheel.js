import Phaser from 'phaser'

const slices = 8,
    slicePrizes = ["A KEY!!!", "50 STARS", "500 STARS", "BAD LUCK!!!", "200 STARS", "100 STARS", "150 STARS", "BAD LUCK!!!"];

export default class extends Phaser.Group {

  constructor (state = [1,1,1,1,1,1,1,1],game, parent, name, addToStage, enableBody, physicsBodyType) {
    super(game, parent, name, addToStage, enableBody, physicsBodyType);
    //this.anchor.setTo(0.5);
    this.state = state;
      this.wheelSprite = this.create(0, 0, "wheel");
      this.wheelSprite.anchor.set(0.5);

      this.pin = this.create(0,0, "pin");
      // setting pin registration point in its center
      this.pin.anchor.set(0.5);


      this.sprites = state.map((e,i) => this.addChild(new Phaser.Text(game, Math.cos(i*2*Math.PI/state.length)*200, Math.sin(i*2*Math.PI/state.length)*200, e)));
      this.sprites.forEach(e => {e.anchor.set(.5); console.log(e.anchor)});
    //console.log(this.sprites);

    // this.centerX = game.width / 2;
    // this.centerY = game.height / 2;

      this.canSpin = true;
      this.prizeText = game.add.text(this.game.world.centerX, 480, "");
      // setting text field registration point in its center
      this.prizeText.anchor.set(0.5);
      // aligning the text to center
      this.prizeText.align = "center";
      // the game has just started = we can spin the wheel
      // waiting for your input, then calling "spin" function


  }

  spin() {

      // can we spin the wheel?
      if(this.canSpin){
          // resetting text field
          this.prizeText.text = "";
          // the wheel will spin round from 2 to 4 times. This is just coreography
          let rounds = this.game.rnd.between(2, 4);
          // then will rotate by a random number from 0 to 360 degrees. This is the actual spin
          let degrees = this.game.rnd.between(0, 360);
          console.log('Deg:',degrees);
          // before the wheel ends spinning, we already know the prize according to "degrees" rotation and the number of slices
          this.prize = slices - 1 - Math.floor(degrees / (360 / slices));
          console.log('prize:',this.prize);
          // now the wheel cannot spin because it's already spinning
          this.canSpin = false;
          // animation tweeen for the spin: duration 3s, will rotate by (360 * rounds + degrees) degrees
          // the quadratic easing will simulate friction
          let spinTween = this.game.add.tween(this).to({
              angle: 360 * rounds + degrees
          }, 3000, Phaser.Easing.Quadratic.Out, true);
          // once the tween is completed, call winPrize function
          spinTween.onComplete.add(this.doPrize, this);
      }

  }

    doPrize() {
    console.log(this.sprites[this.prize]);
        let prizeTween = this.game.add.tween(this.sprites[this.prize]).to({
            angle: 0,
            x:0,
            y:0,
        }, 1000, Phaser.Easing.Quadratic.Out, true);
        prizeTween.onComplete.add(this.winPrize, this);
    }


    winPrize(){
        //this.sprites[this.prize].color('#ffffff');
        // now we can spin the wheel again
        this.canSpin = true;
        // writing the prize you just won
        this.prizeText.text = slicePrizes[this.prize];
    }

  update () {
    //this.angle += 1
      //console.log(this.angle);
      this.pin.angle = - this.angle;
      this.sprites.forEach((e,i) => {e.rotation = Math.PI/2 + i*(Math.PI*2)/this.state.length});
  }

}
